class Peer:
    def __init__(self, name, host_port, peer_socket, status = True, server_port = 0) -> None:
        self.name = name
        self.port = host_port
        self.peer_socket = peer_socket
        self.status = status
        self.server_port = server_port
