import socket
import threading
import random
import re

name = input("Choose your name: ")

peer_server_ip = socket.gethostbyname(socket.gethostname())
peer_server_port = random.randrange(48621,49151)

peer_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
peer_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
host_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
status = True

host_client.connect((socket.gethostbyname(socket.gethostname()), 48620))

peer_server.bind((peer_server_ip, peer_server_port))
peer_server.listen()

def sendBack(message):
    peer_client.send(message)

def handle(peer2_client):
    while True:
        try:
            message = peer2_client.recv(1024)
            sendBack(message)
        except:
            print("An error occured! *handle()")
            peer2_client.close()
            break

def peerServerReceive(peer2_client, peer2_address, peer2_name):
    while True:     
        print("Connected with {}".format(str(peer2_address)))
        sendBack(f"{peer2_name} joined!".encode('ascii'))
        peer2_client.send(f'Start conversation with {name}!'.encode('ascii'))

        thread = threading.Thread(target=handle, args=(peer2_client,))
        thread.start()

def peerClientWrite():
    while True:
        message = '{}: {}'.format(name, input(''))
        peer_client.send(message.encode('ascii'))

def peerClientReceive(peer2_client):
    while True:
        try:
            message = peer_client.recv(1024).decode('ascii')
            if message == '$End$':
                peer_client.close()
            else:
                print(message)
        except:
            print("An error occured! *peerClientReceive()")
            peer_client.close()
            break

def startConvo(peer2_client, peer2_address, peer2_name):
    peerServerReceiveThread = threading.Thread(target=peerServerReceive, args=(peer2_client, peer2_address, peer2_name,))
    peerServerReceiveThread.start()
    
    peerClientReceiveThread = threading.Thread(target=peerClientReceive, args=(peer2_client, ))
    peerClientReceiveThread.start()
    
    peerClientWriteThread = threading.Thread(target=peerClientWrite, args=())
    peerClientWriteThread.start()
        

def hostClientReceive():
    # Host client receive from host
    while True:
        try:
            message = host_client.recv(1024).decode('ascii')
            print(message)
            if message == 'INIT':
                host_client.send(name.encode('ascii'))
            elif message == 'CONVO':
                host_client.send(f'serverport:{peer_server_port}_name:{name}'.encode('ascii'))
                global status 
                status = False
                # Allow accept
                peer2_client, peer2_address = peer_server.accept()
                # Receive peer 2 info
                message = peer_client.recv(1024).decode('ascii')
                peer2_server_info = re.split('_|:', message)
                peer_client.connect((peer2_server_info[1], peer2_server_info[3]))
                
                startConvo(peer2_client, peer2_address, peer2_server_info[5])
            else:
                print(message)
        except:
            print("An error occured! *hostClientReceive()")
            host_client.close()
            break

def hostClientWrite():
    while True:
        message = '{}: {}'.format(name, input(''))
        host_client.send(message.encode('ascii'))

hostClientReceiveThread = threading.Thread(target=hostClientReceive)
hostClientReceiveThread.start()

hostClientWriteThread = threading.Thread(target=hostClientWrite)
hostClientWriteThread.start()
